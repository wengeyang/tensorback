import ctypes
import os
import sys
import numpy as np
import platform

# if platform.system() == 'Linux':
#     cur_path = sys.path[0]
#     dll_path = os.path.join(cur_path, "tensorwolf", "kernel.so")
#     c_kernel = ctypes.CDLL(dll_path)
# else:
#     cur_path = os.path.dirname(__file__)
#     dll_path = os.path.join(cur_path, "c_conv", "x64", "Release", "c_conv.dll")
#     work = ctypes.CDLL(dll_path)
work = ctypes.CDLL('./c_conv.so')


def zero_padding(ori, up, down, left, right):
    s = np.zeros([ori.shape[0], ori.shape[1] + up + down, ori.shape[2] + left + right, ori.shape[3]])
    s[:, up:up + ori.shape[1], left:left + ori.shape[2], :] = ori[:, :, :, :]
    return s


def get_pointer(inputs):
    return inputs.ctypes.data_as(ctypes.POINTER(ctypes.c_float))


def conv2d(input, filter, strides, padding):
    batches = input.shape[0]
    i_h = input.shape[1]
    i_w = input.shape[2]
    i_c = input.shape[3]
    f_h = filter.shape[0]
    f_w = filter.shape[1]
    assert i_c == filter.shape[2]
    o_c = filter.shape[3]
    if padding == 'SAME':
        output = np.zeros((batches, i_h, i_w, o_c), dtype=np.float32)
        o_h = i_h
        o_w = i_w
        z_h = (i_h - 1) * strides[1] + f_h
        z_w = (i_w - 1) * strides[2] + f_w
        z = zero_padding(ori=input, up=(z_h - i_h) // 2, down=(z_h - i_h + 1) // 2, left=(z_w - i_w) // 2, right=(z_w - i_w + 1) // 2)
    elif padding == 'VALID':
        o_h = (i_h - f_h + strides[1] - 1) // strides[1] + 1
        o_w = (i_w - f_w + strides[2] - 1) // strides[2] + 1
        output = np.zeros((batches, o_h, o_w, o_c), dtype=np.float32)
        z_h = i_h
        z_w = i_w
        z = input
    else:
        raise NotImplementedError
    z = z.astype(np.float32)
    filter = filter.astype(np.float32)
    assert work.conv2d(get_pointer(z), batches, z_h, z_w, i_c, strides[1], strides[2], get_pointer(filter), f_h, f_w, o_c, get_pointer(output), o_h, o_w) == 0
    return output

def conv2d_gradient(inputs, gradient, ori_filter):
    batches = inputs.shape[0]
    i_h = inputs.shape[1]
    i_w = inputs.shape[2]
    i_c = inputs.shape[3]
    f = gradient
    f_h = f.shape[1]
    f_w = f.shape[2]
    o_c = f.shape[3]
    o_h = ori_filter.shape[0]
    o_w = ori_filter.shape[1]
    z_h = i_h + o_h - 1
    z_w = i_w + o_w - 1
    z = zero_padding(ori=inputs, up=(z_h - i_h) // 2, down=(z_h - i_h + 1) // 2, left=(z_w - i_w) // 2, right=(z_w - i_w + 1) // 2)
    output = np.zeros((o_h, o_w, i_c, o_c), dtype=np.float32)
    z = z.astype(np.float32)
    f = f.astype(np.float32)
    assert work.conv2d_gradient(get_pointer(z), batches, z_h, z_w, i_c, get_pointer(f), f_h, f_w, o_c, get_pointer(output), o_h, o_w) == 0
    return output


def max_pool_gradient(gradient, output, inputs, ksize, strides):
    g = gradient.astype(np.float32)
    input32 = inputs.astype(np.float32)
    work.max_pool_gradient(get_pointer(g), gradient.shape[0], gradient.shape[1], gradient.shape[2], gradient.shape[3], get_pointer(output), ksize[1], ksize[2], get_pointer(input32), inputs.shape[1], inputs.shape[2])
