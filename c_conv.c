int conv2d (float* z, int batches, int z_h, int z_w, int i_c, int h_step, int w_step, float* f, int f_h, int f_w, int o_c, float* o, int o_h, int o_w)
{
	int o_b_size = o_h * o_w * o_c;
	int o_oh_size = o_w * o_c;
	int f_fh_size = f_w * i_c * o_c;
	int f_fw_size = i_c * o_c;
	int z_b_size = z_h * z_w * i_c;
	int z_oh_size = h_step * z_w * i_c;
	int z_ow_size = w_step * i_c;
	int z_fh_size = z_w * i_c;
	float *o_b = o, *z_b = z;
	for (int b = 0; b < batches; b++)
	{
		float *o_oh = o_b, *z_oh = z_b;
		for (int oh = 0; oh < o_h; oh++)
		{
			float *o_ow = o_oh, *z_ow = z_oh;
			for (int ow = 0; ow < o_w; ow++)
			{
				float *f_fh = f, *z_fh = z_ow;
				for (int fh = 0; fh < f_h; fh++)
				{
					float *f_fw = f_fh, *z_fw = z_fh;
					for (int fw = 0; fw < f_w; fw++)
					{
						float *f_ic = f_fw;
						for (int ic = 0; ic < i_c; ic++)
						{
							for (int oc = 0; oc < o_c; oc++)
							{
								o_ow[oc] += z_fw[ic] * f_ic[oc];
							}
							f_ic += o_c;
						}
						f_fw += f_fw_size;
						z_fw += i_c;
					}
					f_fh += f_fh_size;
					z_fh += z_fh_size;
				}
				o_ow += o_c;
				z_ow += z_ow_size;
			}
			o_oh += o_oh_size;
			z_oh += z_oh_size;
		}
		o_b += o_b_size;
		z_b += z_b_size;
	}
	return 0;
}

int conv2d_gradient (float* z, int batches, int z_h, int z_w, int i_c, float* f, int f_h, int f_w, int o_c, float* o, int o_h, int o_w)
{
	int o_oh_size = o_w * i_c * o_c;
	int o_ow_size = i_c * o_c;
	int z_b_size = z_h * z_w * i_c;
	int z_oh_size = z_w * i_c;
	int z_fh_size = z_w * i_c;
	int f_b_size = f_h * f_w * o_c;
	int f_fh_size = f_w * o_c;
	float *z_b = z, *f_b = f;
	for (int b = 0; b < batches; b++)
	{
		float *o_oh = o, *z_oh = z_b;
		for (int oh = 0; oh < o_h; oh++)
		{
			float *o_ow = o_oh, *z_ow = z_oh;
			for (int ow = 0; ow < o_w; ow++)
			{
				float *z_fh = z_ow, *f_fh = f_b;
				for (int fh = 0; fh < f_h; fh++)
				{
					float *z_fw = z_fh, *f_fw = f_fh;
					for (int fw = 0; fw < f_w; fw++)
					{
						float *o_ic = o_ow;
						for (int ic = 0; ic < i_c; ic++)
						{
							for (int oc = 0; oc < o_c; oc++)
							{
								o_ic[oc] += z_fw[ic] * f_fw[oc];
							}
							o_ic += o_c;
						}
						z_fw += i_c;
						f_fw += o_c;
					}
					z_fh += z_fh_size;
					f_fh += f_fh_size;
				}
				o_ow += o_ow_size;
				z_ow += i_c;
			}
			o_oh += o_oh_size;
			z_oh += z_oh_size;
		}
		z_b += z_b_size;
		f_b += f_b_size;
	}
	return 0;
}

int max_pool_gradient(float* g, int batches, int g_h, int g_w, int i_c, float* o, int h_step, int w_step, float* z, int z_h, int z_w)
{
	int g_b_size = g_h * g_w * i_c;
	int g_gh_size = g_w * i_c;
	int z_b_size = z_h * z_w * i_c;
	int z_gh_size = h_step * z_w * i_c;
	int z_gw_size = w_step * i_c;
	int z_h_size = z_w * i_c;
	for (int b = 0; b < batches; b++)
	{
		float *g_b = g + b * g_b_size, *z_b = z + b * z_b_size;
		for (int gh = 0; gh < g_h; gh++)
		{
			float *g_gh = g_b + gh * g_gh_size, *z_gh = z_b + gh * z_gh_size;
			for (int gw = 0; gw < g_w; gw++)
			{
				float *g_gw = g_gh + gw * i_c, *z_gw = z_gh + gw * z_gw_size;
				for (int ic = 0; ic < i_c; ic++)
				{
					float *z_ic = z_gw + ic, *max_loc = z_ic;
					for (int h = 0; h < h_step; h++)
					{
						float *z_h = z_ic + h * z_h_size;
						for (int w = 0; w < w_step; w++)
						{
							float *z_w = z_h + w * i_c;
							if ((*z_w) > (*max_loc))
							{
								max_loc = z_w;
							}
						}
					}
					o[max_loc - z] += g_gw[ic];
				}
			}
		}
	}
}