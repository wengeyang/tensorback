import numpy as np
import test_conv as work

float32 = np.float32
float64 = np.float64
zeros = np.zeros

variable_new_node = {}


class Node(object):
    """Node in a computation graph."""

    def __init__(self):
        """Constructor, new node is indirectly created by Op object __call__ method.

            Instance variables
            ------------------
            self.inputs: the list of input nodes.
            self.op: the associated op object,
                e.g. add_op object if this node is created by adding two other nodes.
            self.const_attr: the add or multiply constant,
                e.g. self.const_attr=5 if this node is created by x+5.
            self.name: node name for debugging purposes.
        """
        self.inputs = []
        self.op = None
        self.const_attr = None
        self.name = ""

    def __add__(self, other):
        """Adding two nodes return a new node."""
        if isinstance(other, Node):
            new_node = add_op(self, other)
        else:
            # Add by a constant stores the constant in the new node's const_attr field.
            # 'other' argument is a constant
            new_node = add_op(self, constant(other))
        return new_node

    def __mul__(self, other):
        """TODO: Your code here"""
        if isinstance(other, Node):
            new_node = mul_op(self, other)
        else:
            new_node = mul_op(self, constant(other))
        return new_node

    # Allow left-hand-side add and multiply.
    __radd__ = __add__
    __rmul__ = __mul__

    def __str__(self):
        """Allow print to display node name."""
        return self.name

    __repr__ = __str__

    def __sub__(self, rhs):
        if isinstance(rhs, Node):
            new_node = sub_op(self, rhs)
        else:
            new_node = sub_op(self, constant(rhs))
        return new_node

    def __rsub__(self, lhs):
        if isinstance(lhs, Node):
            new_node = sub_op(lhs, self)
        else:
            new_node = sub_op(constant(lhs), self)
        return new_node

    def __truediv__(self, rhs):
        if isinstance(rhs, Node):
            new_node = div_op(self, rhs)
        else:
            new_node = div_op(self, constant(rhs))
        return new_node

    def __rtruediv__(self, lhs):
        if isinstance(lhs, Node):
            new_node = div_op(lhs, self)
        else:
            new_node = div_op(constant(lhs), self)
        return new_node

    __floordiv__ = __truediv__
    __rfloordiv__ = __rtruediv__

    def __neg__(self):
        """-node_self, return a new node."""
        new_node = constant(0) - self
        return new_node

    def eval(self, feed_dict={}):
        """Calculate the value of this node by running a session immediately."""
        ex = Executor(eval_node_list=[self])
        return ex.run(feed_dict=feed_dict)[0]

    run = eval


class Op(object):
    """Op represents operations performed on nodes."""

    def __call__(self):
        """Create a new node and associate the op object with the node.
        Returns
        -------
        The new node object.
        """
        new_node = Node()
        new_node.op = self
        return new_node

    def compute(self, node, input_vals):
        """Given values of input nodes, compute the output value.

        Parameters
        ----------
        node: node that performs the compute.
        input_vals: values of input nodes.

        Returns
        -------
        An output value of the node.
        """
        raise NotImplementedError

    def gradient(self, node, output_grad):
        """Given value of output gradient, compute gradient contributions to each input node.

        Parameters
        ----------
        node: node that performs the gradient.
        output_grad: value of output gradient summed from children nodes' contributions

        Returns
        -------
        A list of gradient contributions to each input node respectively.
        """
        raise NotImplementedError


class AddOp(Op):
    """Op to element-wise add two nodes."""

    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "(%s+%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        """Given values of two input nodes, return result of element-wise addition."""
        assert len(input_vals) == 2
        return input_vals[0] + input_vals[1]

    def gradient(self, node, output_grad):
        """Given gradient of add node, return gradient contributions to each input."""
        return [adapt(output_grad, node.inputs[0]), adapt(output_grad, node.inputs[1])]


class MulOp(Op):
    """Op to element-wise multiply two nodes."""

    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "(%s*%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        """Given values of two input nodes, return result of element-wise multiplication."""
        """TODO: Your code here"""
        assert len(input_vals) == 2
        return input_vals[0] * input_vals[1]

    def gradient(self, node, output_grad):
        """Given gradient of multiply node, return gradient contributions to each input."""
        """TODO: Your code here"""
        return [adapt(node.inputs[1] * output_grad, node.inputs[0]),
                adapt(node.inputs[0] * output_grad, node.inputs[1])]


class SubOp(Op):
    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "(%s-%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        assert len(input_vals) == 2
        output = input_vals[0] - input_vals[1]
        return output

    def gradient(self, node, output_grad):
        return [adapt(output_grad, node.inputs[0]), adapt(constant(0.) - output_grad, node.inputs[1])]


class DivOp(Op):
    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "(%s/%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        assert len(input_vals) == 2
        return input_vals[0] / input_vals[1]

    def gradient(self, node, output_grad):
        return [adapt(output_grad / node.inputs[1], node.inputs[0]),
                adapt(((output_grad * node.inputs[0] * constant(-1)) / node.inputs[1]) / node.inputs[1], node.inputs[1])]               


class ExpOp(Op):
    def __call__(self, node_A):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.name = "Exp(%s)" % (node_A.name)
        return new_node

    def compute(self, node, input_vals):
        assert len(input_vals) == 1
        return np.exp(input_vals[0])

    def gradient(self, node, output_grad):
        return [output_grad * exp(node.inputs[0])]


class LogOp(Op):
    def __call__(self, node_A):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.name = "Log(%s)" % (node_A.name)
        return new_node

    def compute(self, node, input_vals):
        assert len(input_vals) == 1
        return np.log(input_vals[0])

    def gradient(self, node, output_grad):
        return [output_grad / node.inputs[0]]


class SqrtOp(Op):
    def __call__(self, node_A):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.name = "Sqrt(%s)" % (node_A.name)
        return new_node

    def compute(self, node, input_vals):
        return np.sqrt(input_vals[0])

    def gradient(self, node, output_grad):
        raise NotImplementedError


class PowOp(Op):
    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "Pow(%s,%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        return np.power(input_vals[0], input_vals[1])

    def gradient(self, node, output_grad):
        raise NotImplementedError


class MatMulOp(Op):
    """Op to matrix multiply two nodes."""

    def __call__(self, node_A, node_B, trans_A=False, trans_B=False):
        """Create a new node that is the result a matrix multiple of two input nodes.

        Parameters
        ----------
        node_A: lhs of matrix multiply
        node_B: rhs of matrix multiply
        trans_A: whether to transpose node_A
        trans_B: whether to transpose node_B

        Returns
        -------
        Returns a node that is the result a matrix multiple of two input nodes.
        """
        new_node = Op.__call__(self)
        new_node.matmul_attr_trans_A = trans_A
        new_node.matmul_attr_trans_B = trans_B
        new_node.inputs = [node_A, node_B]
        new_node.name = "MatMul(%s,%s,%s,%s)" %(node_A.name, node_B.name, str(trans_A), str(trans_B))
        return new_node

    def compute(self, node, input_vals):
        """Given values of input nodes, return result of matrix multiplication."""
        """TODO: Your code here"""
        if node.matmul_attr_trans_A:
            now_A = input_vals[0].T
        else:
            now_A = input_vals[0]
        if node.matmul_attr_trans_B:
            now_B = input_vals[1].T
        else:
            now_B = input_vals[1]
        return np.dot(now_A, now_B)

    def gradient(self, node, output_grad):
        """Given gradient of multiply node, return gradient contributions to each input.
        Useful formula: if Y=AB, then dA=dY B^T, dB=A^T dY
        """
        """TODO: Your code here"""
        A = node.inputs[0]
        B = node.inputs[1]
        d_Y = output_grad
        return [matmul_op(d_Y, B, False, True), matmul_op(A, d_Y, True, False)]


class PlaceholderOp(Op):
    """Op to feed value to a nodes."""

    def __call__(self, dtype, shape = None, name = None):
        """Creates a variable node."""
        new_node = Op.__call__(self)
        new_node.const_attr = (shape, dtype)
        new_node.name = "Placeholder"
        return new_node

    def compute(self, node, input_vals):
        """No compute function since node value is fed directly in Executor."""
        assert False, "placeholder values provided by feed_dict"

    def gradient(self, node, output_grad):
        """No gradient function since node has no inputs."""
        return None


class ZerosLikeOp(Op):
    """Op that represents a constant np.zeros_like."""

    def __call__(self, node_A):
        """Creates a node that represents a np.zeros array of same shape as node_A."""
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.name = "Zeroslike(%s)" % node_A.name
        return new_node

    def compute(self, node, input_vals):
        """Returns zeros_like of the same shape as input."""
        assert(isinstance(input_vals[0], np.ndarray))
        return np.zeros(input_vals[0].shape)

    def gradient(self, node, output_grad):
        return [zeroslike_op(node.inputs[0])]


class OnesLikeOp(Op):
    """Op that represents a constant np.ones_like."""

    def __call__(self, node_A):
        """Creates a node that represents a np.ones array of same shape as node_A."""
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.name = "Oneslike(%s)" % node_A.name
        return new_node

    def compute(self, node, input_vals):
        """Returns ones_like of the same shape as input."""
        assert(isinstance(input_vals[0], np.ndarray))
        return np.ones(input_vals[0].shape)

    def gradient(self, node, output_grad):
        return [zeroslike_op(node.inputs[0])]


class globalvariablesinitializerOp(Op):
    def __call__(self):
        """Creates a node that represents a np.ones array of same shape as node_A."""
        new_node = Op.__call__(self)
        new_node.name = "Global_Variables_Initializer"
        return new_node

    def compute(self, node, input_vals):
        """Returns ones_like of the same shape as input."""
        for key, val in variable_new_node.items():
            if isinstance(val, Node):
                key.const_attr = val.const_attr
            else:
                key.const_attr = val

        return None

    def gradient(self, node, output_grad):
        assert False, "global variables initializer has no gradient"


class VariableOp(Op):
    """docstring for  Variable"""

    def __call__(self, initial_val, dtype=None, shape=None, name=None):
        new_node = Op.__call__(self)
        if shape is not None:
            assert shape == initial_val.shape
        if dtype is not None:
            if isinstance(initial_val, np.ndarray):
                variable_new_node[new_node] = initial_val.astype(dtype)
            else:
                variable_new_node[new_node] = np.array(initial_val).astype(dtype)
        else:
            variable_new_node[new_node] = initial_val
        new_node.name = "Variable"
        return new_node

    def compute(self, node, input_vals):
        return node.const_attr

    def gradient(self, node, input_vals):
        return None


class BroadcastToOp(Op):
    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "BroadcastTo(%s,%s.shape)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        output = input_vals[0]
        if len(output.shape) < len(input_vals[1].shape):
            check = True
            for dim, in_size in enumerate(output.shape):
                if input_vals[1].shape[dim] != in_size:
                    check = False
                    break
            new_shape = output.shape
            if check:
                while len(new_shape) < len(input_vals[1].shape):
                    new_shape = new_shape + (1,)
            output.resize(new_shape)
        output = np.broadcast_to(output, input_vals[1].shape)
        return output

    def gradient(self, node, output_grad):
        grad_A = reduceshapesum(output_grad, node.inputs[0])
        grad_B = zeroslike_op(node.inputs[1])
        return [grad_A, grad_B]


class ReduceSumOp(Op):

    def __call__(self, node_A, axis=None, keep_dims=False, reduction_indices=None):
        new_node = Op.__call__(self)
        if axis is None and reduction_indices is not None:
            axis = tuple(reduction_indices)
        new_node.inputs = [node_A]
        new_node.const_attr = (axis, keep_dims)
        new_node.name = "ReduceSum(%s, axis=%s, keep_dims=%s)" % (
            node_A, axis, keep_dims)
        return new_node

    def compute(self, node, input_vals):
        return np.sum(input_vals[0], axis=node.const_attr[0], keepdims=node.const_attr[1])

    def gradient(self, node, output_grad):
        return [broadcast_to(output_grad, node.inputs[0])]


class ReduceMeanOp(Op):

    def __call__(self, node_A, axis=None, keepdims=False, reduction_indices=None):
        new_node = Op.__call__(self)
        if axis is None and reduction_indices is not None:
            axis = tuple(reduction_indices)
        new_node.inputs = [node_A]
        new_node.const_attr = (axis, keepdims)
        new_node.name = "ReduceMean(%s, axis=%s, keepdims=%s)" % (
            node_A, axis, keepdims)
        return new_node

    def compute(self, node, input_vals):
        assert len(input_vals) == 1
        return np.mean(input_vals[0], axis=node.const_attr[0], keepdims=node.const_attr[1])

    def gradient(self, node, output_grad):
        return [adapt(broadcastto_op(output_grad, node.inputs[0]) / reduce_sum(oneslike_op(node.inputs[0]), axis=node.const_attr[0], keep_dims=node.const_attr[1]), node.inputs[0])]


class AdaptShapeOp(Op):
    def __call__(self, node_A, node_B):
        new_node = reduceshapesum(node_A, node_B)
        new_node.name = "Adapt(%s, %s.shape)" % (
            node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        raise NotImplementedError

    def gradient(self, node, output_grad):
        raise NotImplementedError


class ReduceSumShapeOp(Op):

    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "ReduceShapeSum(%s, %s.shape)" % (
            node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        output = input_vals[0]
        while len(output.shape) > len(input_vals[1].shape):
            output = np.sum(output, axis=0)
        for dim in range(len(output.shape)):
            if output.shape[dim] > input_vals[1].shape[dim]:
                output = np.sum(output, axis=dim, keepdims=True)

        return output

    def gradient(self, node, output_grad):
        return [broadcastto_op(output_grad, node.inputs[0]), zeroslike_op(node.inputs[1])]


class ConstOp(Op):

    def __call__(self, initial_val, dtype=None, shape=None):
        new_node = Op.__call__(self)
        if not isinstance(initial_val, np.ndarray) and(shape is not None):
            initial_val = np.ones(shape=shape) * initial_val
        new_node.const_attr = np.array(initial_val).reshape(shape).astype(dtype)
        new_node.name = "Const"
        return new_node

    def compute(self, node, input_vals):
        return node.const_attr

    def gradient(self, node, output_grad):
        return None


class AssignOp(Op):
    """docstring for assignOp"""

    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        if not isinstance(node_B, Node):
            node_B = constant(node_B)
        new_node.inputs = [node_B]
        new_node.const_attr = node_A
        new_node.name = "(%s:=%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        node.const_attr.const_attr = input_vals[0]
        return input_vals[0]

    def gradient(self, node, output_grad):
        raise NotImplementedError


class ArgMaxOp(Op):

    def __call__(self, node_A, axis=None):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.const_attr = axis
        new_node.name = "Argmax(%s, axis=%s)" % (node_A.name, axis)
        return new_node

    def compute(self, node, input_vals):
        return np.argmax(input_vals[0], axis=node.const_attr)

    def gradient(self, node, output_grad):
        raise NotImplementedError


class EqualOp(Op):

    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "(%s==%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        return np.equal(input_vals[0], input_vals[1])

    def gradient(self, node, output_grad):
        raise NotImplementedError


class CastOp(Op):

    def __call__(self, node_A, dtype):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.const_attr = dtype
        new_node.name = "Cast(%s, dtype=%s)" % (node_A.name, dtype)
        return new_node

    def compute(self, node, input_vals):
        return input_vals[0].astype(node.const_attr)

    def gradient(self, node, input_vals):
        raise NotImplementedError


class PackOp(Op):
    def __call__(self, node_list):
        new_node = Op.__call__(self)
        new_node.inputs = node_list
        new_node.name = "Pack(%s)" % (str([node.name for node in node_list]))
        return new_node

    def compute(self, node, input_vals):
        return None

    def gradient(self, node, output_grad):
        raise NotImplementedError


class ReluGradientOp(Op):
    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "ReluGradient(%s)" % (node_A.name)
        return new_node

    def compute(self, node, input_vals):
        return (np.sign(input_vals[0]) + 1) * 0.5 * input_vals[1]

    def gradient(self, ndoe, output_grad):
        raise NotImplementedError


class ReluOp(Op):
    def __call__(self, node_A):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.name = "Relu(%s)" % (node_A.name)
        return new_node

    def compute(self, node, input_vals):
        return np.maximum(input_vals[0], 0)

    def gradient(self, node, output_grad):
        return [relu_gradient_op(node.inputs[0], output_grad)]


class ReshapeOp(Op):

    def __call__(self, node_A, shape):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.const_attr = shape
        new_node.name = "Reshape(%s, shape=%s)" % (node_A.name, shape)
        return new_node

    def compute(self, node, input_vals):
        return np.reshape(input_vals[0], tuple(node.const_attr))

    def gradient(self, node, output_grad):
        return [reshape_extend(output_grad, node.inputs[0])]


class ReshapeExtendOp(Op):

    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "Reshape(%s, shape=%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        return np.reshape(input_vals[0], input_vals[1].shape)

    def gradient(self, node, output_grad):
        return [reshape_extend(output_grad, node.inputs[0]), zeroslike_op(node.inputs[1])]


def cal_softmax(x):
    expx = np.exp(x - np.max(x, axis=-1, keepdims=True))
    softmax = expx / np.sum(expx, axis=-1, keepdims=True)
    return softmax


class SoftmaxOp(Op):
    def __call__(self, node_A):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.name = "Softmax(%s)" % (node_A.name)
        return new_node

    def compute(self, node, input_vals):
        return cal_softmax(input_vals[0])

    def gradient(self, node, output_grad):
        raise NotImplementedError


class SoftmaxCrossEntropyOp(Op):
    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "SoftmaxCrossEntropy(%s, %s)" % (
            node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        y = input_vals[0]
        y_ = input_vals[1]
        num = cal_softmax(y)
        mid1 = y_ * np.log(num)
        return -np.sum(mid1, axis=-1, keepdims=True)

    def gradient(self, node, output_grad):
        grad_A = (softmax_op(node.inputs[0]) - node.inputs[1]) * output_grad
        grad_B = zeroslike_op(node.inputs[1])
        return [grad_A, grad_B]


def random_normal(shape, mean=0.0, stddev=1.0, dtype=None, seed=None, name=None):
    return_val = np.random.normal(
        loc=mean, scale=stddev, size=shape).astype(dtype)
    return return_val


def zero_padding(ori, up, down, left, right):
    s = np.zeros([ori.shape[0], ori.shape[1] + up + down, ori.shape[2] + left + right, ori.shape[3]])
    s[:, up:up + ori.shape[1], left:left + ori.shape[2], :] = ori[:, :, :, :]
    return s


def get_patch(ori, i, j, f_h, f_w, strides, i_c=None):
    if i_c is None:
        return ori[:, i * strides[1]:i * strides[1] + f_h, j * strides[2]:j * strides[2] + f_w, :]
    else:
        return ori[:, i * strides[1]:i * strides[1] + f_h, j * strides[2]:j * strides[2] + f_w, i_c]


class Conv2dOp(Op):
    def __call__(self, node_A, node_B, strides=[1, 1, 1, 1], padding='SAME'):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.const_attr = (strides, padding)
        new_node.name = "Conv2D(%s, filter=%s)" % (node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        output = work.conv2d(input=input_vals[0], filter=input_vals[1], strides=node.const_attr[0], padding=node.const_attr[1])
        return output

    def gradient(self, node, output_grad):
        return [conv2d_g_A(node.inputs[0], node.inputs[1], output_grad, node.const_attr), conv2d_g_B(node.inputs[0], node.inputs[1], output_grad, node.const_attr)]


class Conv2dGradientAOp(Op):

    def __call__(self, node_A, node_B, node_grad, strpad):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B, node_grad]
        new_node.const_attr = strpad
        new_node.name = "Conv2dGradientA"
        return new_node

    def compute(self, node, input_vals):
        return work.conv2d(input=input_vals[2], filter=np.rot90(np.transpose(input_vals[1], (0, 1, 3, 2)), axes=(0, 1), k=2), strides=[1, 1, 1, 1], padding=node.const_attr[1])

    def gradient(self, node, output_grad):
        raise NotImplementedError


class Conv2dGradientBOp(Op):

    def __call__(self, node_A, node_B, node_grad, strpad):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B, node_grad]
        new_node.const_attr = strpad
        new_node.name = "Conv2dGradientB"
        return new_node

    def compute(self, node, input_vals):
        return work.conv2d_gradient(inputs=input_vals[0], gradient=input_vals[2], ori_filter=input_vals[1])

    def gradient(self, node, output_grad):
        raise NotImplementedError


class MaxpoolOp(Op):

    def __call__(self, node_A, ksize, strides, padding='SAME'):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A]
        new_node.const_attr = (ksize, strides, padding)
        new_node.name = "MaxPool(%s)" % (node_A.name)
        return new_node

    def compute(self, node, input_vals):
        batches = input_vals[0].shape[0]
        i_h = input_vals[0].shape[1]
        i_w = input_vals[0].shape[2]
        i_c = input_vals[0].shape[3]
        strides = node.const_attr[0]
        ksize = node.const_attr[1]
        padding = node.const_attr[2]
        o_h = (i_h - 1) // strides[1] + 1
        o_w = (i_w - 1) // strides[2] + 1
        if padding == 'SAME':
            z_h = ((i_h - 1) // strides[1]) * strides[1] + ksize[1]
            z_w = ((i_w - 1) // strides[2]) * strides[2] + ksize[2]
            z = zero_padding(ori=input_vals[0], up=(z_h - i_h) // 2, down=(z_h - i_h + 1) // 2, left=(z_w - i_w) // 2, right=(z_w - i_w + 1) // 2)
        else:
            raise NotImplementedError
        output = np.zeros([batches, o_h, o_w, i_c])
        for i in range(o_h):
            for j in range(o_w):
                output[:, i, j, :] = np.max(get_patch(z, i, j, ksize[1], ksize[2], strides), axis=(1, 2))
        return output

    def gradient(self, node, output_grad):
        return [max_pool_g(node.inputs[0], output_grad, node.const_attr)]


class MaxpoolGradientOp(Op):

    def __call__(self, node_A, node_B, const_attr):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.const_attr = const_attr
        new_node.name = "MaxpoolGradient"
        return new_node

    def compute(self, node, input_vals):
        batches = input_vals[0].shape[0]
        i_h = input_vals[0].shape[1]
        i_w = input_vals[0].shape[2]
        i_c = input_vals[0].shape[3]
        strides = node.const_attr[1]
        ksize = node.const_attr[0]
        padding = node.const_attr[2]
        if padding == 'SAME':
            z_h = ((i_h - 1) // strides[1]) * strides[1] + ksize[1]
            z_w = ((i_w - 1) // strides[2]) * strides[2] + ksize[2]
            z = zero_padding(ori=input_vals[0], up=(z_h - i_h) // 2, down=(z_h - i_h + 1) // 2, left=(z_w - i_w) // 2, right=(z_w - i_w + 1) // 2)
        else:
            raise NotImplementedError
        output = np.zeros((batches, z_h, z_w, i_c), dtype=np.float32)
        work.max_pool_gradient(gradient=input_vals[1], inputs=z, output=output, ksize=ksize, strides=strides)
        up = (z_h - i_h) // 2
        left = (z_w - i_w) // 2
        output = output[:, up:up + i_h, left:left + i_w, :]
        return output

    def gradient(self, node, output_grad):
        raise NotImplementedError


class ProbshapeOp(Op):

    def __call__(self, node_A, node_B):
        new_node = Op.__call__(self)
        new_node.inputs = [node_A, node_B]
        new_node.name = "ProbShape(shape=%s,prob=%s)" % (
            node_A.name, node_B.name)
        return new_node

    def compute(self, node, input_vals):
        output = (np.random.uniform(size=input_vals[0].shape) < input_vals[1])
        return output

    def gradient(self, node, output_grad):
        return [zeroslike_op(node.inputs[0]), zeroslike_op(node.inputs[1])]


class nn(object):

    class SoftmaxOp(Op):

        def __call__(self, node_A, axis=-1):
            exp_node = exp_op(node_A)
            new_node = exp_node / broadcast_to(reduce_sum(exp_node, axis=axis), exp_node)
            return new_node

    class SoftMaxCrossOp(Op):

        def __call__(self, logits, labels):
            return softmax_cross_entropy_op(logits, labels)

    softmax = SoftmaxOp()
    relu = ReluOp()
    softmax_cross_entropy_with_logits = SoftMaxCrossOp()
    conv2d = Conv2dOp()
    max_pool = MaxpoolOp()

    class DropoutOp(Op):

        def __call__(self, node_A, node_B):
            new_node = mul_op(node_A, probshape_op(node_A, node_B)) / node_B
            return new_node

    dropout = DropoutOp()

# Create global singletons of operators.


add_op = AddOp()
mul_op = MulOp()
sub_op = SubOp()
div_op = DivOp()
exp_op = ExpOp()
log_op = LogOp()
exp = ExpOp()
log = LogOp()
sqrt_op = SqrtOp()
pow_op = PowOp()
matmul_op = MatMulOp()
matmul = MatMulOp()
oneslike_op = OnesLikeOp()
zeroslike_op = ZerosLikeOp()
placeholder = PlaceholderOp()
global_variables_initializer = globalvariablesinitializerOp()
Variable = VariableOp()
reduce_sum = ReduceSumOp()
reduce_mean = ReduceMeanOp()
assign = AssignOp()
broadcast_to = BroadcastToOp()
broadcastto_op = BroadcastToOp()
constant = ConstOp()
adapt = AdaptShapeOp()
reduceshapesum = ReduceSumShapeOp()
argmax = ArgMaxOp()
equal = EqualOp()
cast = CastOp()
pack = PackOp()
relu_gradient_op = ReluGradientOp()
softmax_cross_entropy_op = SoftmaxCrossEntropyOp()
softmax_op = SoftmaxOp()
conv2d_op = Conv2dOp()
conv2d_g_A = Conv2dGradientAOp()
conv2d_g_B = Conv2dGradientBOp()
max_pool = MaxpoolOp()
max_pool_g = MaxpoolGradientOp()
probshape_op = ProbshapeOp()
reshape = ReshapeOp()
reshape_extend = ReshapeExtendOp()


def find_topo_sort(node_list):
    """Given a list of nodes, return a topological sort list of nodes ending in them.
    A simple algorithm is to do a post-order DFS traversal on the given nodes, 
    going backwards based on input edges. Since a node is added to the ordering
    after all its predecessors are traversed due to post-order DFS, we get a topological
    sort.

    """
    visited = set()
    topo_order = []
    for node in node_list:
        topo_sort_dfs(node, visited, topo_order)
    return topo_order


def topo_sort_dfs(node, visited, topo_order):
    """Post-order DFS"""
    if node in visited:
        return
    visited.add(node)
    for n in node.inputs:
        topo_sort_dfs(n, visited, topo_order)
    topo_order.append(node)


class Executor(object):
    """Executor computes values for given set of nodes in computation graph."""

    def __init__(self, eval_node_list):
        """
        Parameters
        ----------
        eval_node_list: list of nodes whose values need to be computed.
        topo_order: list of nodes in topological order
        """
        self.eval_node_list = eval_node_list
        self.topo_order = find_topo_sort(self.eval_node_list)

    # profile
    def run(self, feed_dict):
        """
        Parameters
        ----------
        feed_dict: a dictionary of node->np.ndarray supplied by user.

        Returns
        -------
        A list of values for nodes in eval_node_list. NDArray or np.ndarray.
        """
        node_to_val_map = {}
        for node, value in feed_dict.items():
            node_to_val_map[node] = np.array(value)

        for node in self.topo_order:
            if node in node_to_val_map:
                continue
            input_vals = [node_to_val_map[n] for n in node.inputs]
            value = node.op.compute(node, input_vals)
            if not isinstance(value, np.ndarray) and value is not None:
                value = np.array(value)
            node_to_val_map[node] = value

        return [node_to_val_map[n] for n in self.eval_node_list]


def gradients(output_node, node_list):
    """Take gradient of output node with respect to each node in node_list.

    Parameters
    ----------
    output_node: output node that we are taking derivative of.
    node_list: list of nodes that we are taking derivative wrt.

    Returns
    -------
    A list of gradient values, one for each node in node_list respectively.

    """
    oneslike_op = OnesLikeOp()
    node_to_output_grads_list = {}
    node_to_output_grads_list[output_node] = [oneslike_op(output_node)]
    node_to_output_grad = {}
    reverse_topo_order = reversed(find_topo_sort([output_node]))
    for node in reverse_topo_order:
        output_grad = sum_node_list(node_to_output_grads_list[node])
        node_to_output_grad[node] = output_grad
        input_grads_list = node.op.gradient(node, output_grad)
        for i in range(len(node.inputs)):
            if node.inputs[i] not in node_to_output_grads_list:
                node_to_output_grads_list[node.inputs[i]] = []
            node_to_output_grads_list[node.inputs[i]].append(
                input_grads_list[i])
    grad_node_list = [node_to_output_grad[node] for node in node_list]
    return grad_node_list


def sum_node_list(node_list):
    """Custom sum func to avoid creating redundant nodes in Python sum func."""
    from operator import add
    from functools import reduce
    return reduce(add, node_list)


def sum_node_list(node_list):
    """Custom sum function in order to avoid create redundant nodes in Python sum implementation."""
    from operator import add
    from functools import reduce
    return reduce(add, node_list)


class Session(object):
    """docstring for Session"""

    def __init__(self, target='', graph=None, config=None):
        self.target = target
        self.graph = graph
        self.config = config

    def run(self, eval_node_list, feed_dict={}):

        isList = True
        if not isinstance(eval_node_list, list):
            isList = False
            eval_node_list = [eval_node_list]
        self.ex = Executor(eval_node_list=eval_node_list)
        if isList:
            return self.ex.run(feed_dict=feed_dict)
        else:
            return self.ex.run(feed_dict=feed_dict)[0]

    def __exit__(self, e_t, e_v, t_b):
        return self

    def __enter__(self):
        return self


class train(object):

    class Optimizer(object):
        def __init__(self):
            return None

        def get_variables_list(self):
            variables_list = []
            for variable in variable_new_node:
                variables_list.append(variable)
            return variables_list

    class GradientDescentOptimizer(Optimizer):
        def __init__(self, learning_rate=0.01):
            self.learning_rate = learning_rate

        def minimize(self, target):
            variables_prepare = self.get_variables_list()
            variables_to_change = []
            used_ones = find_topo_sort(node_list=[target])
            for v in variables_prepare:
                if v in used_ones:
                    variables_to_change.append(v)
            variables_gradients = gradients(target, variables_to_change)
            change_list = []
            for index, variable in enumerate(variables_to_change):
                change_list.append(assign(variable, variable - (self.learning_rate * variables_gradients[index])))
            return pack(change_list)

    class AdamOptimizer(Optimizer):
        def __init__(self, learning_rate=0.001, beta1=0.9, beta2=0.999, epsilon=1e-8):
            self.learning_rate = learning_rate
            self.beta1 = beta1
            self.beta2 = beta2
            self.epsilon = epsilon

        def minimize(self, target):
            variables = self.get_variables_list()
            gradient = gradients(target, variables)
            change_list = []
            self.t = constant(0)
            self.assignt = assign(self.t, self.t + 1)
            self.lrt = self.learning_rate * sqrt_op(1 - pow_op(constant(self.beta2), self.assignt)) / (1 - pow_op(constant(self.beta1), self.assignt))
            self.m = []
            self.assignm = []
            self.v = []
            self.assignv = []
            for variable in variables:
                self.m.append(constant(0))
                self.v.append(constant(0))
            for index, variable in enumerate(variables):
                g = gradient[index]
                nw_m = self.m[index]
                mt = assign(nw_m, nw_m * self.beta1 + g * (1 - self.beta1))
                nw_v = self.v[index]
                vt = assign(nw_v, nw_v * self.beta2 + g * g * (1 - self.beta2))
                new_val = variable  - self.lrt * mt / (sqrt_op(vt) + constant(self.epsilon))
                change_list.append(assign(variable, new_val))
            return pack(change_list)
